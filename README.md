# Exercice React Class Component - Création de composants React sous forme de classes

## Objectif
Créer des composants en React qui utilise les classes. Le but sera de manipuler plusieurs composants et transmettre des props de l'un à l'autre en travaillant sur la notion de mise à jour du state, de transmissions de props et d'utilisation du destructuring.

## Prérequis
Assurez-vous d'avoir Node.js installé sur votre machine.    

## Étapes

### Étape 1: Initialisation du projet React 

1. Ouvrez votre terminal et créez un nouveau projet React avec la commande suivante :
   ```bash
   npx create-react-app exo-react-cc
   ```

2. Accédez au répertoire du projet :
   ```bash 
   cd exo-react-cc
   ```

3. Lancez l'installation des modules et construction du dossier public :
   ```bash
   npm install
   ```

4. Lancez l'application pour vous assurer que tout fonctionne correctement :
   ```bash
   npm start
   ```  

### Étape 2: Création des composants
1. Créez un dossier components à l'intérieur du dossier src.

2. Dans le dossier components, créez les composants suivants :

- Parent
- ChildrenOne 
- ChildrenTwo 

### Étape 3: Instructions   

- Parent : Doit être un composant appelé dans App.js qui contiendra le state fournis dans l'Etape 4. 

- ChildrenOne : Sera un composant affichant les informations firstName et country provenant du state Parent et contiendra en dessous un bouton affichant le texte 'Show' si le code secret est masqué ou 'Hide' si il est affiché dans le composant ChildrenTwo.
Ce bouton au click déclenchera l'affichage des informations secretes de la personne dans le composant ChildrenTwo en passant la variable du Parent à isShowable = true.  

- ChildrenTwo : Le composant est appelé dans le composant ChildrenOne et doit afficher les informations suivantes firstName, lastName, age, city et 'Mariée' ou 'Non Mariée".
Ce composant doit avoir un evénement qui analyse les dimensions de la fenêtre du navigateur en temps réel et permettre l'affichage des informations secretes uniquement sur la largeur de la fenêtre est supérieur à 70% de la taille du moniteur en changeant le boolean isAuthorizeToRevealCode avec une fonction toggle. 

Assurez-vous d'inclure le code source complet pour la vérification. 

### Étape 4: State   
Voici le contenu du state dont vous aurez besoin pour réaliser cet exercice:
```ruby
this.state = {
      firstName: "Emilia",
      lastName: "Alvez Garcia",
      age: 32,
      isMarried: true,
      nationality: "Spanish",
      country: "Spain",
      city: "Murcia",
      isAuthorizeToRevealCode: false,
      isShowable: false,
      secretCode: "54FZ90ER42"
    };
```

### Étape 5: Conditions de réussite
Le projet se lance sur le port 3000.    
Les informations uniquement décrites dans l'etape 3 sont présentes à l'écran.     
Il doit y avoir un observeur d'evenement pour la taille de la fenetre et au moins une fonction toggle.
Le secretCode affiche ********** si il est masqué ou que la taille de la fenetre est inférieur à 70%. 
Le composants ChildrenTwo ne doit recevoir que les props dont il a besoin et doit disposer d'un affichage conditionnel.

### Étape 6: Vérification
Le candidat devra fournir l'ensemble des fichiers à l'exception des node_modules après avoir complété la tâche. 

### Étape 7: Styles (bonus)
Ajoutez des styles supplémentaires avec les fonctionnalités fournies par votre bibliothèque graphique si vous le souhaitez.

### Aperçu graphique
Voici une représentation de ce qui est attendu graphiquement, vous avez la possibilité de modifier un peu celui-ci.

![Representation graphique de l'exercice](/images/exo-react-cc.png)

### Remarques
- Afin de répondre aux meilleurs pratiques, déclarez vos variables de préférences avec const et let.
- Privilégiez la syntax des fonctions en Arrow Function.  
- N'oubliez pas de gérer les erreurs potentielles si un state ou une props est manquant ou non défini.   
- Vous avez le droit d'ajouter des fonctionnalités au projet (loader, tests, TypeScript etc...)
- Utilisez les composants de manière modulaire et réutilisable.   
- Commentez votre code pour expliquer votre démarche si c'est pertinent.


### Ressource
Documentation React : https://reactjs.org/docs/getting-started.html         

### Soumission
Lorsque vous avez terminé, assurez-vous de fournir le code source du projet pour évaluation soit en l'envoyant au format ZIP sans y inclure les node_modules par [E-mail](mailto:nilpa018@yahoo.fr) soit en l'hébergeant sur un Drive afin de pouvoir télécharger celui-ci.
